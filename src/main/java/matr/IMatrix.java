package matr;

import matrix.errors.MatrixException;

public interface IMatrix {
    double getElementAt(int i, int j) throws MatrixException;

    void setElementAt(int i, int j, double value) throws MatrixException;

    double determinant() throws MatrixException;
}
