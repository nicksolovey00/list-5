package matr;

import matrix.errors.MatrixException;

import java.util.Comparator;

public class MatrixComparator implements Comparator<Matrix> {
    public int compare(Matrix m1, Matrix m2) {
        try {
            return Double.compare(m1.determinant(), m2.determinant());
        } catch (MatrixException e) {
            throw new RuntimeException();
        }
    }
}
