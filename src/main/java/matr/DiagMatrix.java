package matr;

import matrix.errors.MatrixErrorCode;
import matrix.errors.MatrixException;

import java.util.Arrays;

public class DiagMatrix extends Matrix implements IMatrix {

    public DiagMatrix(int n) throws MatrixException {
        if (n <= 0){
            throw new MatrixException(MatrixErrorCode.WRONG_SIZE);
        }
        matrix = new double[n];
        Arrays.fill(matrix, 0);
        det = 0;
        correctDet = true;
        length = n;
    }

    /* Филиппов А.В. 28.11.2020 Комментарий не удалять.
     Не работает. Нет проверки корректности столбца и строки.
    */
    @Override
    public void setElementAt(int i, int j, double value) throws MatrixException {
        if(i != j && value != 0){
            throw new MatrixException(MatrixErrorCode.NOT_DIAGONAL);
        }
        if(i < 0 || j < 0 || i >= matrix.length){
            throw new MatrixException(MatrixErrorCode.WRONG_INDEX);
        }
        matrix[i] = value;
        correctDet = false;
    }

    /* Филиппов А.В. 28.11.2020 Комментарий не удалять.
     Не работает. Нет проверки корректности столбца и строки.
    */
    @Override
    public double getElementAt(int i, int j) throws MatrixException {
        if (i != j){
            return 0;
        }
        if(i < 0 || j < 0 || i >= matrix.length){
            throw new MatrixException(MatrixErrorCode.WRONG_INDEX);
        }
        return matrix[i];
    }

    @Override
    public void printMatrix() throws MatrixException {
        for (int i = 0; i < length; i++){
            for(int j = 0; j < length; j++){
                System.out.print(getElementAt(i, j) + " ");
            }
            System.out.println();
        }
    }

    @Override
    public double determinant() throws MatrixException {
        if (correctDet){
            return det;
        }

        det = getElementAt(0, 0);
        for (int i = 1; i < length; i++){
            det *= getElementAt(i, i);
        }
        correctDet = true;
        return det;
    }
}
