package matr;

import java.util.Arrays;

public class MatrixService {
    public static void sortByDeterminant(Matrix[] matrices){
        Arrays.sort(matrices, new MatrixComparator());
    }
}
