package matr;

import matrix.errors.MatrixErrorCode;
import matrix.errors.MatrixException;

import java.util.Arrays;
import java.util.Objects;

public class Matrix implements IMatrix{
    protected double[] matrix;
    protected int length;
    protected double det;
    protected boolean correctDet;

    public Matrix(int n) throws MatrixException {
        if (n <= 0){
            throw new MatrixException(MatrixErrorCode.WRONG_SIZE);
        }
        matrix = new double[n*n];
        Arrays.fill(matrix, 0);
        det = 0;
        correctDet = true;
        length = n;
    }

    /* Филиппов А.В. 28.11.2020 Комментарий не удалять.
     Скорей всего public нужно заменить на protected
    */
    protected Matrix() {

    }

    public void printMatrix() throws MatrixException {
        for(int i = 0; i < length; i++){
            for(int j = 0; j < length; j++){
                System.out.print(getElementAt(i, j) + " ");
            }
            System.out.println();
        }
    }

    private void swap(int first, int second) throws MatrixException {
        if (first != second){
            for (int i = 0; i < length; i++) {
                double temp = getElementAt(i, first);
                setElementAt(i, first, getElementAt(i, second));
                setElementAt(i, second, temp);
            }
        }
    }




    /* Филиппов А.В. 28.11.2020 Комментарий не удалять.
     Не работает. См. тест.
    */
    public double getElementAt(int i, int j) throws MatrixException {
        if(i >= length || j >= length || i < 0 || j < 0){
            throw new MatrixException(MatrixErrorCode.WRONG_INDEX);
        }
        return matrix[i*length + j];
    }

    /* Филиппов А.В. 28.11.2020 Комментарий не удалять.
     Не работает. См. тест.
    */
    public void setElementAt(int i, int j, double value) throws MatrixException {
        if(i >= length || j >= length || i < 0 || j < 0){
            throw new MatrixException(MatrixErrorCode.WRONG_INDEX);
        }
        matrix[i*length + j] = value;
        correctDet = false;
    }

    public int length(){
        return length;
    }

    /* Филиппов А.В. 28.11.2020 Комментарий не удалять.
     Не работает. См. тест.
    */
    public double determinant() throws MatrixException {
        if (correctDet){
            return det;
        }
        double[][] matrixCopy = new double[length][length];
        for (int i = 0; i < matrixCopy.length; i++){
            for (int j = 0; j < matrixCopy.length; j++){
                matrixCopy[i][j] = getElementAt(i, j);
            }
        }

        if (matrix.length == 1){
            return matrixCopy[0][0];
        }

        int minus = 1;
        if (matrixCopy[0][0] == 0){
            boolean nullCol = true;
            for (int i = 1; i < matrixCopy.length; i++){
                if(matrixCopy[0][i] != 0){
                    swap(0, i);
                    nullCol = false;
                    minus *= -1;
                    for (int j = 0; j < matrixCopy.length; j++){
                        for (int k = 0; k < matrixCopy.length; k++){
                            matrixCopy[j][k] = getElementAt(j, k);
                        }
                    }
                    break;
                }
            }
            if (nullCol){
                return 0;
            }
        }

        Matrix temp = new Matrix(matrixCopy.length - 1);
        for (int i = 0; i < temp.length(); i++){
            for (int j = 0; j < temp.length(); j++){
                temp.setElementAt(i, j, matrixCopy[i+1][j+1]);
            }
        }

        for(int i = 0; i < temp.length(); i++){
            if (matrixCopy[i+1][0] != 0) {
                for (int j = 0; j < temp.length(); j++) {
                    temp.setElementAt(i, j, matrixCopy[i+1][j+1] - matrixCopy[0][j+1]/getElementAt(0,0)*matrixCopy[i+1][0]);
                }
            }
        }

        correctDet = true;
        det = minus*matrixCopy[0][0]*temp.determinant();
        return det;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Matrix matrix1 = (Matrix) o;
        return Double.compare(matrix1.det, det) == 0 &&
                correctDet == matrix1.correctDet &&
                Arrays.equals(matrix, matrix1.matrix);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(det, correctDet);
        result = 31 * result + Arrays.hashCode(matrix);
        return result;
    }

    @Override
    public String toString() {
        return "Matrix{" +
                "matrix=" + Arrays.toString(matrix) +
                '}';
    }
}
