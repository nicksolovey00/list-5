package matrix.errors;

public class MatrixException extends Exception{
    MatrixErrorCode matrixErrorCode;

    public MatrixException(MatrixErrorCode matrixErrorCode) {
        this.matrixErrorCode = matrixErrorCode;
    }
}
