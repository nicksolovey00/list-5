package matrix.errors;

public enum MatrixErrorCode {
    WRONG_INDEX("No element at this index"),
    NOT_DIAGONAL("Element at this index is not at diagonal"),
    UNDER_DIAGONAL("Element at this index is under diagonal of up triangle matrix"),
    WRONG_SIZE("The size of matrix under or equals zero");


    private String error;

    MatrixErrorCode(String error) {
        this.error = error;
    }
}
