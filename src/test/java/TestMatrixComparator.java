import matr.Matrix;
import matr.MatrixComparator;
import matrix.errors.MatrixException;
import org.junit.Assert;
import org.junit.Test;

public class TestMatrixComparator {
    @Test
    public void testMatrixComparator() throws MatrixException {
        Matrix firstMatrix = new Matrix(2);
        Matrix secondMatrix =  new Matrix(2);
        Matrix thirdMatrix = new Matrix(2);

        firstMatrix.setElementAt(0, 0, 11);
        firstMatrix.setElementAt(0, 1, -2);
        firstMatrix.setElementAt(1, 0, 7);
        firstMatrix.setElementAt(1, 1, 5);

        secondMatrix.setElementAt(0, 0, 1);
        secondMatrix.setElementAt(0, 1, 3);
        secondMatrix.setElementAt(1, 0, -2);
        secondMatrix.setElementAt(1, 1, 5);

        thirdMatrix.setElementAt(0, 0, 1);
        thirdMatrix.setElementAt(0, 1, 3);
        thirdMatrix.setElementAt(1, 0, -2);
        thirdMatrix.setElementAt(1, 1, 5);

        Assert.assertEquals(1, new MatrixComparator().compare(firstMatrix, secondMatrix));
        Assert.assertEquals(-1, new MatrixComparator().compare(secondMatrix, firstMatrix));
        Assert.assertEquals(0, new MatrixComparator().compare(thirdMatrix, secondMatrix));
    }
}
