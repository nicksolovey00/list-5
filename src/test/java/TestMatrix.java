import matr.Matrix;
import matrix.errors.MatrixException;
import org.junit.Assert;
import org.junit.Test;

public class TestMatrix {
    final double DELTA = 1e-10;
    @Test
    public void testMatrix() throws MatrixException {
        Matrix matrix = new Matrix(3);
        matrix.setElementAt(0, 0, 1);
        matrix.setElementAt(1, 1, 2);
        matrix.setElementAt(2, 2, 3);
        matrix.setElementAt(2, 0, 5);
        matrix.setElementAt(2, 0, 5);
        matrix.printMatrix();
    }

    @Test (expected = MatrixException.class)
    public void testMatrixException1() throws MatrixException {
        Matrix matrix = new Matrix(-3);
    }

    @Test (expected = MatrixException.class)
    public void testMatrixException2() throws MatrixException {
        Matrix matrix = new Matrix(3);
        matrix.setElementAt(5, 5, 5);
    }

    /* Филиппов А.В. 28.11.2020 Комментарий не удалять.
     Не работает.
     Он и так выбрасывает npe. Зачем мне еще свое заводить?
    */
   @Test(expected = MatrixException.class)
    public void testMatrixIdx() throws MatrixException {
        Matrix matrix = new Matrix(3);
        matrix.setElementAt(3, 3, 1);
        Assert.assertEquals(1, matrix.getElementAt(3, 3), 1e-9);
    }

    /* Филиппов А.В. 28.11.2020 Комментарий не удалять.
     Не работает.
    */
    @Test(expected = MatrixException.class)
    public void testMatrixIdx2() throws MatrixException {
        Matrix matrix = new Matrix(3);
        matrix.setElementAt(1, -1, 1);
        Assert.assertEquals(1, matrix.getElementAt(1, -1), 1e-9);
    }

    @Test
    public void testDeterminant() throws MatrixException {
        Matrix matrixTest1 = new Matrix(2);
        matrixTest1.setElementAt(0, 0, 11);
        matrixTest1.setElementAt(0, 1, -3);
        matrixTest1.setElementAt(1, 0, -15);
        matrixTest1.setElementAt(1, 1, -2);
        Assert.assertEquals(-67, matrixTest1.determinant(), DELTA);


        Matrix matrixTest2 = new Matrix(3);
        matrixTest2.setElementAt(0, 0, 3);
        matrixTest2.setElementAt(0, 1, 3);
        matrixTest2.setElementAt(0, 2, -1);
        matrixTest2.setElementAt(1, 0, 4);
        matrixTest2.setElementAt(1, 1, 1);
        matrixTest2.setElementAt(1, 2, 3);
        matrixTest2.setElementAt(2, 0, 1);
        matrixTest2.setElementAt(2, 1, -2);
        matrixTest2.setElementAt(2, 2, -2);
        Assert.assertEquals(54, matrixTest2.determinant(), DELTA);


        Matrix matrixTest3 = new Matrix(3);
        matrixTest3.setElementAt(0, 0, 1);
        matrixTest3.setElementAt(0, 1, -2);
        matrixTest3.setElementAt(0, 2, 3);
        matrixTest3.setElementAt(1, 0, 4);
        matrixTest3.setElementAt(1, 1, 0);
        matrixTest3.setElementAt(1, 2, 6);
        matrixTest3.setElementAt(2, 0, -7);
        matrixTest3.setElementAt(2, 1, 8);
        matrixTest3.setElementAt(2, 2, 9);
        Assert.assertEquals(204, matrixTest3.determinant(), DELTA);


        Matrix matrixTest4 = new Matrix(4);
        matrixTest4.setElementAt(0, 0, 3);
        matrixTest4.setElementAt(0, 1, -3);
        matrixTest4.setElementAt(0, 2, -5);
        matrixTest4.setElementAt(0, 3, 8);
        matrixTest4.setElementAt(1, 0, -3);
        matrixTest4.setElementAt(1, 1, 2);
        matrixTest4.setElementAt(1, 2, 4);
        matrixTest4.setElementAt(1, 3, -6);
        matrixTest4.setElementAt(2, 0, 2);
        matrixTest4.setElementAt(2, 1, -5);
        matrixTest4.setElementAt(2, 2, -7);
        matrixTest4.setElementAt(2, 3, 5);
        matrixTest4.setElementAt(3, 0, -4);
        matrixTest4.setElementAt(3, 1, 3);
        matrixTest4.setElementAt(3, 2, 5);
        matrixTest4.setElementAt(3, 3, -6);
        Assert.assertEquals(18, matrixTest4.determinant(), DELTA);
    }

    @Test
    public void testZeroColumn() throws MatrixException {
        Matrix matrixTest = new Matrix(4);
        matrixTest.setElementAt(0, 0, 0);
        matrixTest.setElementAt(0, 1, -3);
        matrixTest.setElementAt(0, 2, -5);
        matrixTest.setElementAt(0, 3, 8);
        matrixTest.setElementAt(1, 0, 0);
        matrixTest.setElementAt(1, 1, 2);
        matrixTest.setElementAt(1, 2, 4);
        matrixTest.setElementAt(1, 3, -6);
        matrixTest.setElementAt(2, 0, 0);
        matrixTest.setElementAt(2, 1, -5);
        matrixTest.setElementAt(2, 2, -7);
        matrixTest.setElementAt(2, 3, 5);
        matrixTest.setElementAt(3, 0, 0);
        matrixTest.setElementAt(3, 1, 3);
        matrixTest.setElementAt(3, 2, 5);
        matrixTest.setElementAt(3, 3, -6);
        Assert.assertEquals(0, matrixTest.determinant(), DELTA);
    }

    @Test
    public void testDet() throws MatrixException {
        Matrix matrixTest3 = new Matrix(3);
        matrixTest3.setElementAt(0, 0, 0);
        matrixTest3.setElementAt(0, 1, 1);
        matrixTest3.setElementAt(0, 2, 1);
        matrixTest3.setElementAt(1, 0, 0);
        matrixTest3.setElementAt(1, 1, 0);
        matrixTest3.setElementAt(1, 2, 1);
        matrixTest3.setElementAt(2, 0, 1);
        matrixTest3.setElementAt(2, 1, 1);
        matrixTest3.setElementAt(2, 2, 1);
        Assert.assertEquals(1, matrixTest3.determinant(), DELTA);
    }
}
