import matr.DiagMatrix;
import matrix.errors.MatrixException;
import org.junit.Assert;
import org.junit.Test;

public class TestDiagMatrix {
    @Test
    public void testDiagMatrix() throws MatrixException {
        DiagMatrix diagMatrix = new DiagMatrix(3);
        diagMatrix.setElementAt(0, 0, 1);
        diagMatrix.setElementAt(1, 1, 1);
        diagMatrix.setElementAt(2, 2, 1);
        diagMatrix.printMatrix();
    }

    @Test
    public void testSetElemAt() throws MatrixException {
        DiagMatrix diagMatrix = new DiagMatrix(3);
        try {
            diagMatrix.setElementAt(2, 0, 1);
        } catch (MatrixException e) {
            return;
        }
    }

    @Test
    public void testDeterminant() throws MatrixException {
        DiagMatrix diagMatrix = new DiagMatrix(4);
        diagMatrix.setElementAt(0, 0, 5);
        diagMatrix.setElementAt(1, 1, 5);
        diagMatrix.setElementAt(2, 2, 5);
        diagMatrix.setElementAt(3, 3, 1);
        diagMatrix.printMatrix();
        Assert.assertEquals(125, diagMatrix.determinant(), 1e-9);
    }
}
