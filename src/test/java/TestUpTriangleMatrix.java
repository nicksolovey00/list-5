import matr.UpTriangleMatrix;
import matrix.errors.MatrixException;
import org.junit.Assert;
import org.junit.Test;

public class TestUpTriangleMatrix {
    @Test
    public void testUpTriangleMatrix() throws MatrixException {
        UpTriangleMatrix upTriangleMatrix = new UpTriangleMatrix(2);
        upTriangleMatrix.setElementAt(0, 0, 1);
        upTriangleMatrix.setElementAt(0, 1, 1);
        upTriangleMatrix.setElementAt(1, 1, 1);
        try {
            upTriangleMatrix.setElementAt(1, 0, 1);
        } catch (MatrixException e) {
            upTriangleMatrix.printMatrix();
            return;
        }
    }

    @Test
    public void testDeterminant() throws MatrixException {
        UpTriangleMatrix upTriangleMatrix = new UpTriangleMatrix(2);
        upTriangleMatrix.setElementAt(0, 0, 8);
        upTriangleMatrix.setElementAt(0, 1, 1);
        upTriangleMatrix.setElementAt(1, 1, 7);
        Assert.assertEquals(56, upTriangleMatrix.determinant(), 1e-10);
    }
}
