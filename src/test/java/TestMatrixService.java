import matr.DiagMatrix;
import matr.Matrix;
import matr.MatrixService;
import matr.UpTriangleMatrix;
import matrix.errors.MatrixException;
import org.junit.Assert;
import org.junit.Test;

public class TestMatrixService {
    @Test
    public void testSortByDeterminant() throws MatrixException {
        Matrix firstMatrix = new Matrix(2);//67
        DiagMatrix secondMatrix = new DiagMatrix(3);//25
        UpTriangleMatrix thirdMatrix = new UpTriangleMatrix(2);//1

        firstMatrix.setElementAt(0, 0, 11);
        firstMatrix.setElementAt(0, 1, -2);
        firstMatrix.setElementAt(1, 0, 7);
        firstMatrix.setElementAt(1, 1, 5);

        secondMatrix.setElementAt(0, 0, 5);
        secondMatrix.setElementAt(1, 1, 5);
        secondMatrix.setElementAt(2, 2, 1);

        thirdMatrix.setElementAt(0, 0, 1);
        thirdMatrix.setElementAt(0, 1, 1);
        thirdMatrix.setElementAt(1, 1, 1);

        Matrix[] matrices = new Matrix[]{
                firstMatrix,
                secondMatrix,
                thirdMatrix
        };

        Matrix[] expected = new Matrix[]{
                thirdMatrix, secondMatrix, firstMatrix
        };
        MatrixService.sortByDeterminant(matrices);

        Assert.assertArrayEquals(expected, matrices);
    }
}
